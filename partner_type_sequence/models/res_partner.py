from odoo import fields, models, api


class ResUsers(models.Model):
    _inherit = "res.partner"

    is_customer = fields.Boolean()
    is_vendor = fields.Boolean()

    customer_code = fields.Char()
    supplier_code = fields.Char()

    @api.onchange('is_customer')
    def onchange_customer(self):
        for customer in self:
            if not customer.customer_code:
                code = self.env["ir.sequence"].next_by_code("partner.customer")
                customer.customer_code = code

    @api.onchange('is_vendor')
    def onchange_vendor(self):
        for vendor in self:
            if not vendor.supplier_code:
                code = self.env["ir.sequence"].next_by_code("partner.vendor")
                vendor.supplier_code = code

    def name_get(self):
        data = []
        for record in self:
            code = ''
            if record.customer_code and record.is_customer:
                code += '[' + record.customer_code + ']'
            if record.supplier_code and record.is_vendor:
                code += '[' + record.supplier_code + ']'
            name = '%s  %s' % (code, record.name)
            data.append((record.id, name))
        return data

    @api.model
    def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
        args = args or []
        domain = []
        if name:
            domain = ['|', '|', ('name', operator, name), ('customer_code', operator, name), ('supplier_code', operator, name)]
        return self._search(domain + args, limit=limit, access_rights_uid=name_get_uid)
