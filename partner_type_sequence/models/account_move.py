from odoo import fields, models, api


class AccountMove(models.Model):
    _inherit = 'account.move'

    allowed_partner_ids = fields.Many2many('res.partner', compute='compute_allowed_partners')

    @api.depends('move_type')
    def compute_allowed_partners(self):
        for record in self:
            if record.move_type in ('out_invoice', 'out_refund'):
                domain = [('is_customer', '=', True)]
            elif record.move_type in ('in_invoice', 'in_refund'):
                domain = [('is_vendor', '=', True)]
            else:
                domain = []
            partners = self.env['res.partner'].search(domain)
            record.allowed_partner_ids = [(6, 0, partners.ids)]


class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    partner_type = fields.Selection([
        ('customer', 'Customer'),
        ('vendor', 'vendor')
    ])

    allowed_partner_ids = fields.Many2many('res.partner', compute='compute_allowed_partners')

    @api.depends('partner_type')
    def compute_allowed_partners(self):
        for record in self:
            if record.partner_type == 'customer':
                domain = [('is_customer', '=', True)]
            elif record.partner_type == 'vendor':
                domain = [('is_vendor', '=', True)]
            else:
                domain = []
            partners = self.env['res.partner'].search(domain)
            record.allowed_partner_ids = [(6, 0, partners.ids)]
