
{
    "name": "Partner Type Sequence",
    "summary": "Unique Sequence for Partner based on Type",
    "version": "14.0.1",
    "category": "Contacts",
    "website": "",
    "author": "Shafeek",
    "license": "LGPL-3",
    "installable": True,
    "depends": ["base", "sale", "purchase", "account"],
    "data": [
        "data/ir_sequence_data.xml",
        "views/res_partner_views.xml",
        "views/sale_order_views.xml",
        "views/purchase_order_views.xml",
        "views/account_move_views.xml",
    ],
    "sequence": 1,
}
