odoo.define('tree_view_customisation.list_search', function (require) {
    "use strict";

    var ListRenderer = require('web.ListRenderer');
    var core = require('web.core');
    var _t = core._t;
    var rpc = require('web.rpc');

    ListRenderer.include({
        events: _.extend({
            'change .oe_search_input': '_onInputChange'
        }, ListRenderer.prototype.events),
        _renderView: function () {
            var self = this;
            return this._super.apply(this, arguments).then(function () {
                self.$('.o_list_table').addClass('o_list_table_ungrouped');
                if (self.arch.tag === 'tree' && self.$el.hasClass('o_list_view') && self.state.model === 'crm.lead') {
                    var preString = _t('Sales Person: ');
                    var searchInput = $('<select class="oe_search_input mt-2 ml-2 pl-3"><option value="">' + _t('None') + '</option></select>');

                    var $searchContainer = $('<div>').css({
                        'float': 'center',
                        'width': '25%'
                    });
                    $searchContainer.append(preString);
                    $searchContainer.append(searchInput);
                    self.$el.prepend($searchContainer);

                    rpc.query({
                        model: 'res.users',
                        method: 'search_read',
                        domain: [],
                        fields: ['id', 'name'],
                    }).then(function (users) {
                        users.forEach(function (user) {
                            searchInput.append($('<option>', {
                                value: user.id,
                                text: user.name
                            }));
                        });
                    });
                }
            });
        },
        _onInputChange: function (event) {
            var selectedValue = parseInt($(event.currentTarget).val(), 10);
//            setDefaultSearchUserId(selectedValue);
            if (!isNaN(selectedValue)) {
            var self = this;
            return this.do_action({
                name: _t('Pipeline'),
                type: 'ir.actions.act_window',
                res_model: 'crm.lead',
                domain: [['user_id', '=', selectedValue]],
                views: [[false, 'list'], [false, 'form'], [false, 'kanban']],
                view_mode: 'list,form,kanban',
                target: 'current',
                context: {
                    search_default_user_id: selectedValue
                }
            });
    }
        },
    });
});
