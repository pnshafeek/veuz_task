{
    'name': 'Tree View Customisation',
    'version': '14',
    'sequence': 3,
    'category': 'All module',
    'summary': 'Tree View Customisation',
    'description': """Tree View Customisation""",
    "author": "Shafeek",
    'depends': ['base', 'web', 'sale_management', 'crm'],
    'data': [
        'views/assets.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
    'license': 'LGPL-3',
}
