from odoo import api, models, _


class CrmLead(models.Model):
    _inherit = "crm.lead"

    @api.model
    def return_with_filter(self):
        context = self.env.context
        default_search_user_id = context.get('default_search_user_id')
        domain = [('user_id', '=', default_search_user_id)] if default_search_user_id else []

        action = {
            'name': _('Lead or Opportunity'),
            'type': 'ir.actions.act_window',
            'res_model': 'crm.lead',
            'view_mode': 'tree',
            'view_id': self.env.ref('crm.crm_case_tree_view_oppor').id,
            'domain': domain,
            'context': {
                'default_search_user_id': default_search_user_id
            }
        }
        return action

